package org.eclipse.main;



import java.util.ArrayList;
import java.util.List;

import org.eclipse.config.HibernateUtil;
import org.eclipse.dao.AbonnementDao;
import org.eclipse.dao.ClientDao;
import org.eclipse.dao.FactureDao;
import org.eclipse.model.Abonnement;
import org.eclipse.model.AbonnementFixe;
import org.eclipse.model.AbonnementGSM;
import org.eclipse.model.Client;
import org.eclipse.model.Facture;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class App 
{
    public static void main( String[] args )
    {
    	Session session = HibernateUtil.getSessionFactory().openSession(); 
    	
    	//Client
    	Client client1 = new Client("Boris", "bm@gmail.com", "0102030405", "Antibes");
    	Client client2 = new Client("Bea", "bc@gmail.com", "0602030405", "Lilles");
    	
    	//Facture
    	Facture facture1 = new Facture("janvier", 12, true);
    	Facture facture2 = new Facture("fevrier", 15, true);
    	Facture facture3 = new Facture("mars", 14, true);
    	Facture facture4 = new Facture("janvier", 21, true);
    	Facture facture5 = new Facture("fevrier", 18, true);
    	Facture facture6 = new Facture("mars", 19, true);
    	
    	List<Facture> lesFactures1 = new ArrayList<Facture>();
    	lesFactures1.add(facture1);
    	lesFactures1.add(facture2);
    	lesFactures1.add(facture3);
    	
    	List<Facture> lesFactures2 = new ArrayList<Facture>();
    	lesFactures2.add(facture4);
    	lesFactures2.add(facture5);
    	lesFactures2.add(facture6);
    	
    	//Abonnement
    	Abonnement abo1 = new AbonnementGSM("Port_Boris", "janvier 2020", 0, client1, lesFactures1, 2);
    	Abonnement abo2 = new AbonnementFixe("Fixe_Bea", "janvier 2020", 0, client2, lesFactures2, 12);
    	
    	AbonnementDao abonnementDao = new AbonnementDao(session);
    	try {
			abonnementDao.save(abo1);
			abonnementDao.save(abo2);
			System.out.println(abonnementDao.findById(1));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

    	session.close(); 
    	HibernateUtil.bybye();

    }
}
