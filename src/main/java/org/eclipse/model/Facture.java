package org.eclipse.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity

public class Facture {
	
	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
	private Integer idFact;
	private String dateFact;			// a changer en type date
	private float montant;
	private boolean reglee;
	public Integer getIdFact() {
		return idFact;
	}
	
	public String getDateFact() {
		return dateFact;
	}
	public void setDateFact(String dateFact) {
		this.dateFact = dateFact;
	}
	public float getMontant() {
		return montant;
	}
	public void setMontant(float montant) {
		this.montant = montant;
	}
	public boolean isReglee() {
		return reglee;
	}
	public void setReglee(boolean reglee) {
		this.reglee = reglee;
	}
	@Override
	public String toString() {
		return "Facture [idFact=" + idFact + ", dateFact=" + dateFact + ", montant=" + montant + ", reglee=" + reglee
				+ "]";
	}
	public Facture(String dateFact, float montant, boolean reglee) {
		super();
		this.dateFact = dateFact;
		this.montant = montant;
		this.reglee = reglee;
	}
	public Facture() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
