package org.eclipse.model;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Embeddable;
import javax.persistence.Entity;

@Entity 
@DiscriminatorValue(value="GSM") 

public class AbonnementGSM extends Abonnement{

	private int fidelio;

	public int getFidelio() {
		return fidelio;
	}

	public void setFidelio(int fidelio) {
		this.fidelio = fidelio;
	}

	@Override
	public String toString() {
		return super.toString() + "AbonnementGSM [fidelio=" + fidelio + "]";
	}

	public AbonnementGSM(String nomAbonnement, String dateAbonnement, float solde, Client client,
			List<Facture> lesFactures, int fidelio) {
		super(nomAbonnement, dateAbonnement, solde, client, lesFactures);
		this.fidelio = fidelio;
	}

	public AbonnementGSM() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}
