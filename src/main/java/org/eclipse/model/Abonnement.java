package org.eclipse.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE) // une seule table
@DiscriminatorColumn(name="TYPE_ABONNEMENT") 
@DiscriminatorValue(value="PAS")

public class Abonnement {
	
	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
	private Integer idAbonnement;
	private String nomAbonnement;
	private String dateAbonnement;
	private float solde;
	
	@OneToOne(cascade={CascadeType.PERSIST, CascadeType.REMOVE}) 
	@JoinColumn(name="idClient", referencedColumnName="idClient", nullable=false) 
	private Client client;
	
	@OneToMany(cascade={CascadeType.PERSIST, CascadeType.REMOVE}) 
	private List<Facture> lesFactures = new ArrayList<Facture>();

	public Integer getIdAbonnement() {
		return idAbonnement;
	}

	public String getNomAbonnement() {
		return nomAbonnement;
	}

	public void setNomAbonnement(String nomAbonnement) {
		this.nomAbonnement = nomAbonnement;
	}

	public String getDateAbonnement() {
		return dateAbonnement;
	}

	public void setDateAbonnement(String dateAbonnement) {
		this.dateAbonnement = dateAbonnement;
	}

	public float getSolde() {
		return solde;
	}

	public void setSolde(float solde) {
		this.solde = solde;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<Facture> getLesFactures() {
		return lesFactures;
	}

	public void setLesFactures(List<Facture> lesFactures) {
		this.lesFactures = lesFactures;
	}

	@Override
	public String toString() {
		return "Abonnement [idAbonnement=" + idAbonnement + ", nomAbonnement=" + nomAbonnement + ", dateAbonnement="
				+ dateAbonnement + ", solde=" + solde + ", client=" + client + ", lesFactures=" + lesFactures + "]";
	}

	public Abonnement(String nomAbonnement, String dateAbonnement, float solde, Client client,
			List<Facture> lesFactures) {
		super();
		this.nomAbonnement = nomAbonnement;
		this.dateAbonnement = dateAbonnement;
		this.solde = solde;
		this.client = client;
		this.lesFactures = lesFactures;
	}

	public Abonnement() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
