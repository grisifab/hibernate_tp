package org.eclipse.model;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity 
@DiscriminatorValue(value="FIXE") 

public class AbonnementFixe extends Abonnement {

	private int debit;

	public int getDebit() {
		return debit;
	}

	public void setDebit(int debit) {
		this.debit = debit;
	}

	@Override
	public String toString() {
		return super.toString() + "AbonnementFixe [debit=" + debit + "]";
	}

	public AbonnementFixe(String nomAbonnement, String dateAbonnement, float solde, Client client,
			List<Facture> lesFactures, int debit) {
		super(nomAbonnement, dateAbonnement, solde, client, lesFactures);
		this.debit = debit;
	}

	public AbonnementFixe() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
